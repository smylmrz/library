<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Book::factory(20)->create();
        \App\Models\Librarian::factory(2)->create();
        \App\Models\Reader::factory(20)->create();

        for($i = 1; $i <= now()->daysInMonth; $i++) // Create a librarian work shifts for each day of the month 
        {
            $id = $i % 2 == 0 
                ? 1 // First librarion works on odd days
                : 2; // The other librarian works on even days

            \App\Models\Shift::create([
                'librarian_id' => $id,
                'date' => Carbon::createFromDate(now()->year, now()->month, $i)->format('Y-m-d H:i:s')
            ]);
        }

        for($i = 1; $i <= 10; $i++) 
        {
            \App\Models\Borrow::create([
                'reader_id' => rand(1, 20),
                'book_id' => rand(1, 20),
                'librarian_id' => 1,
                'borrow_date' => Carbon::createFromDate(now()->year, now()->month, $i)->format('Y-m-d H:i:s')
            ]);

            \App\Models\Borrow::create([
                'reader_id' => rand(1, 20),
                'book_id' => rand(1, 20),
                'librarian_id' => 2,
                'borrow_date' => Carbon::createFromDate(now()->year, now()->month, $i + 1)->format('Y-m-d H:i:s')
            ]);
        }

    }
}

# Library Management

To install the project follow these steps:

-   Run `composer update` command
-   Create an .env file copy .env.example to it
-   Run `php artisan key:generate` command
-   Run `php artisan migrate:fresh --seed` to migrate tables and seed dummy data. This will create some books associcated with autors, some readers and 2 librarians, and librarian work shifts for the month.

## Readers

-   Get the list of Readers | `GET /api/readers`
-   Create a new reader | `POST /api/readers` with a `name` in the body
-   Update a reader | `PUT /api/readers` with an `id` parameter and a `name` in the body
-   Delete a reader | `DELETE /api/readers` with an `id` parameter

## Librarians

-   Get the list of Librarians | `GET /api/librarians`
-   Get the librarian with the the books given | `GET /api/librarian-of-the-month`

## Borrows

-   Borrowing a book | `POST /api/borrows` with `reader_id`, `book_id`. `librarian_id` will be automaticcaly filled based on a day. `due_date` is in a week from the `borrow_date`.
-   Returning a book | `PUT /api/borrows` with `id` parameter for `borrows` table and a `return_date` in the body.

# Note

I did not create `CRUD` operations for `Books` and `Author` since it's repetitive and unnecessary.

<?php

namespace App\Http\Controllers;

use App\Models\Borrow;
use App\Models\Shift;
use App\Repositories\BorrowRepository;
use Illuminate\Http\Request;

class BorrowController extends Controller
{
    protected $borrow;

    public function __construct(BorrowRepository $borrowRepository)
    {
        $this->borrow = $borrowRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->borrow->getAllBorrows();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $borrow = Borrow::where('reader_id', $request->reader_id)->first();

        return $borrow->return_date > $borrow->due_date
            ? response(['message' => 'This user can not borrow books due to return delays'])
            : $this->borrow->storeBorrow(Shift::currentLibrarianId(), $request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $this->borrow->getBorrow($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        return $this->borrow->updateBorrow($id, $request);
    }
}

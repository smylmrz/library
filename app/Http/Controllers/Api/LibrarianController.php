<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\BorrowRepository;
use App\Repositories\LibrarianRepository;

class LibrarianController extends Controller
{
    protected $librarian;
    protected $borrow;

    public function __construct(LibrarianRepository $librarianRepository, BorrowRepository $borrowRepository)
    {
        $this->librarian = $librarianRepository;    
        $this->borrow = $borrowRepository;
    }

    public function index()
    {
        return $this->librarian->getLibrarians();
    }

    /**
     * Identify the librarian with the most books given
     */
    public function librarianOfTheMonth()
    {
        $maxBorrows = $this->borrow->getMaxBorrows();

        $librarian = $this->librarian->getLibrarian($maxBorrows->librarian_id);

        return response([
            'librarian' => $librarian->name,
            'books_issued' => $maxBorrows->total
        ]);
    }
}

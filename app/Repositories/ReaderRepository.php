<?php

namespace App\Repositories;

use App\Models\Reader;
use Illuminate\Database\Eloquent\Collection;

class ReaderRepository {

    public function all() : Collection
    {
        return Reader::orderBy('name')->get();
    }

    public function show($id) : Reader
    {
        return Reader::find($id);
    }

    public function updateOrCreate($id = null, $collection) : Reader
    {
        return Reader::updateOrCreate(
            [
                'id' => $id
            ],
            [
                'name' => $collection->name
            ]
        );
    }

    public function delete($id) : bool
    {
        return Reader::find($id)->delete();
    }

}
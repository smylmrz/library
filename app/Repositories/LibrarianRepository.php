<?php

namespace App\Repositories;

use App\Models\Librarian;
use Illuminate\Database\Eloquent\Collection;

class LibrarianRepository {
    
    /**
     * Get the list of borrows
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     * 
     */
    public function getLibrarians() : Collection
    {
        return Librarian::all();
    }

    /**
     * Get details about the specified librarian 
     * 
     * @param int $id 
     * @return \App\Models\Librarian
     * 
     */
    public function getLibrarian($id) : Librarian
    {
        return Librarian::find($id);
    }
}
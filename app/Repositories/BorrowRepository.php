<?php

namespace App\Repositories;

use App\Models\Borrow;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class BorrowRepository {
    
    /**
     * Get the list of borrows
     * 
     * @return \Illuminate\Database\Eloquent\Collection
     * 
     */
    public function getAllBorrows() : Collection
    {
        return Borrow::orderByDesc('borrow_date')->get();
    }

    /**
     * Borrow a book from a librarion 
     * 
     * @param int $librarian_id 
     * @param $collection
     * @return \App\Models\Borrow
     * 
     */
    public function storeBorrow($librarian_id, $collection) : Borrow
    {
        return Borrow::create([
            'librarian_id' => $librarian_id,
            'reader_id' => $collection->reader_id,
            'book_id' => $collection->book_id,
        ]);
    }

    /**
     * Borrow a book from a librarion 
     * 
     * @param int $id 
     * @param $collection
     * @return bool
     * 
     */
    public function updateBorrow($id, $collection) : bool
    {
        return Borrow::find($id)->update([
            'return_date' => $collection->return_date
        ]);
    }

    /**
     * Get details about the specified borrow 
     * 
     * @param int $id 
     * @return \App\Models\Borrow
     * 
     */
    public function getBorrow($id) : Borrow
    {
        return Borrow::find($id);
    }

    /**
     * Get total borrows from a single librarian
     * 
     * @return object
     * 
     */
    public function getMaxBorrows() : object
    {
        return DB::table('borrows')
                ->select('librarian_id', DB::raw('count(*) as total'))
                ->groupBy('librarian_id')
                ->orderBy('total', 'desc')
                ->first();
    }
}
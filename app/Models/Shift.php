<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shift extends Model
{
    use HasFactory;

    protected $fillable = ['librarian_id', 'date'];

    protected $dates = ['date'];

    public function ScopeCurrentLibrarianId()
    {
        return $this->whereDate('date', Carbon::today())->first()->librarian_id;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Borrow extends Model
{
    use HasFactory;

    protected $fillable = ['librarian_id', 'book_id', 'reader_id', 'borrow_date', 'due_date', 'return_date'];

    protected $dates = ['borrow_date', 'due_date', 'return_date'];

    public function librarian() : BelongsTo
    {
        return $this->belongsTo(Librarian::class);
    }

}

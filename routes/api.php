<?php

use App\Http\Controllers\Api\LibrarianController;
use App\Http\Controllers\Api\ReaderController;
use App\Http\Controllers\BorrowController;
use Illuminate\Support\Facades\Route;

Route::prefix('readers')->group(function () {
    Route::get('/',        [ReaderController::class, 'index']);
    Route::post('/',       [ReaderController::class, 'store']);
    Route::get('/{id}',    [ReaderController::class, 'show']);
    Route::put('/{id}' ,   [ReaderController::class, 'update']);
    Route::delete('/{id}', [ReaderController::class, 'destroy']);
});

Route::prefix('borrows')->group(function () {
    Route::get('/',     [BorrowController::class, 'index']);
    Route::post('/',    [BorrowController::class, 'store']);
    Route::put('/{id}', [BorrowController::class, 'update']);
});

Route::get('/librarians', [LibrarianController::class, 'index']);
Route::get('/librarian-of-the-month', [LibrarianController::class, 'librarianOfTheMonth']);